variable "worker_firewall_rules" {
  description = "firewall rules for the worker firewall"
  type = list(object({
    protocol   = string
    port       = string
    description = optional(string)
    source_ips = list(string)
  }))
  default = [
    {
      protocol    = "udp"
      port        = "1234"
      description = "Default worker description"
      source_ips  = ["1.2.3.4/32"]
    }
  ]
}

variable "controlplane_firewall_rules" {
  description = "firewall rules for the controlplane firewall"
  type = list(object({
    protocol   = string
    port       = string
    description = optional(string)
    source_ips = list(string)
  }))
  default = [
    {
      protocol    = "udp"
      port        = "1234"
      description = "Default controlplane description"
      source_ips  = ["1.2.3.4/32"]
    }
  ]
}

variable "controlplane_server_ids" {
  description = "IDs of controlplane servers"
  type        = list(string)
}

variable "worker_server_ids" {
  description = "IDs of worker servers"
  type        = list(string)
}
