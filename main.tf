resource "hcloud_firewall" "controlplane" {
  name = "controlplane-firewall"
  dynamic "rule" {
    for_each = var.controlplane_firewall_rules
    content {
      direction  = "in"
      protocol   = rule.value["protocol"]
      port       = rule.value["port"]
      description = rule.value["description"]
      source_ips = rule.value["source_ips"]
    }
  }
}

resource "hcloud_firewall" "worker" {
  name = "worker-firewall"
  dynamic "rule" {
    for_each = var.worker_firewall_rules
    content {
      direction  = "in"
      protocol   = rule.value["protocol"]
      port       = rule.value["port"]
      description = rule.value["description"]
      source_ips = rule.value["source_ips"]
    }
  }
}

resource "hcloud_firewall_attachment" "controlplane" {
  firewall_id = hcloud_firewall.controlplane.id
  server_ids  = var.controlplane_server_ids
}

resource "hcloud_firewall_attachment" "worker" {
  firewall_id = hcloud_firewall.worker.id
  server_ids  = var.worker_server_ids
}