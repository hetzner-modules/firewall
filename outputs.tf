output "controlplane_firewall_name" {
  value = hcloud_firewall.controlplane.name
}

output "worker_firewall_name" {
  value = hcloud_firewall.worker.name
}
