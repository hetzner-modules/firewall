<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.2.0 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | >= 1.33.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_hcloud"></a> [hcloud](#provider\_hcloud) | >= 1.33.2 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [hcloud_firewall.controlplane](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/firewall) | resource |
| [hcloud_firewall.worker](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/firewall) | resource |
| [hcloud_firewall_attachment.controlplane](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/firewall_attachment) | resource |
| [hcloud_firewall_attachment.worker](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/firewall_attachment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_controlplane_firewall_rules"></a> [controlplane\_firewall\_rules](#input\_controlplane\_firewall\_rules) | firewall rules for the worker firewall | <pre>list(object({<br>    protocol   = string<br>    port       = string<br>    source_ips = list(string)<br>  }))</pre> | `[]` | no |
| <a name="input_controlplane_server_ids"></a> [controlplane\_server\_ids](#input\_controlplane\_server\_ids) | IDs of controlplane servers | `list(string)` | n/a | yes |
| <a name="input_worker_firewall_rules"></a> [worker\_firewall\_rules](#input\_worker\_firewall\_rules) | firewall rules for the worker firewall | <pre>list(object({<br>    protocol   = string<br>    port       = string<br>    source_ips = list(string)<br>  }))</pre> | `[]` | no |
| <a name="input_worker_server_ids"></a> [worker\_server\_ids](#input\_worker\_server\_ids) | IDs of worker servers | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_controlplane_firewall_name"></a> [controlplane\_firewall\_name](#output\_controlplane\_firewall\_name) | n/a |
| <a name="output_worker_firewall_name"></a> [worker\_firewall\_name](#output\_worker\_firewall\_name) | n/a |
<!-- END_TF_DOCS -->

